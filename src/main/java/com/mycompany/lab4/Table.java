/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author zacop
 */
public class Table {
    private Player currentPlayer;
    private char table[][] = {{'-','-','-'}, {'-','-','-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private int row,col;
    private int turncount = 0;
    
    Table(Player player1,Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }
    
    public char[][] getTable(){
        return table;
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public boolean setRowCol(int row,int col){
        if(table[row-1][col-1] == '-'){
            turncount++;
            table[row-1][col-1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }return false;
    }
    
    public void switchPlayer(){
        if(currentPlayer == player1){
            currentPlayer = player2;
        }else{
            currentPlayer = player1;
        }
    }
    
    public boolean checkWin(){
        if(checkRow()){
            return true;
        }
        if(checkCol()){
            return true;
        }
        if(checkDiagonal_Right()){
            return true;
        }
        if(checkDiagonal_left()){
            return true;
        }
        return false;
    }
    
    public boolean checkRow(){
        return table[row-1][0]!= '-' && table[row-1][0] == table[row-1][1] && table[row-1][1] == table[row-1][2];
    }
    
    public boolean checkCol(){
        return table[0][col-1]!= '-' && table[0][col-1] == table[1][col-1] && table[1][col-1] == table[2][col-1];
        // table[0][0] == table[1][0] == table[2][0]
    } 
    
    public boolean checkDiagonal_Right(){
        return table[0][0]!= '-' && table[1][1] == table[0][0] && table[1][1] == table[2][2];
    }
    
    public boolean checkDiagonal_left(){
        return table[0][2]!= '-' && table[0][2] == table[1][1] && table[1][1] == table[2][0];
    }
    
    public boolean checkDraw(){
        if(turncount == 9){
            return true;
        }
        return false;
    }
}

